AI Controller Descriptions

Aggressive AI - 
This AI will pursue the player in a certain range and will avoid
obstacles if they block the path. Once they hit half health, they
will attempt to flee.

Cowardly AI -
This AI is the opposite of the Aggressive AI. They will flee withing
a certain range and will change to chase down the player once they
hit half health.

Hunter AI -
This AI will loop always be seeking out a player. It will grab the player,
or a player if it's splitscreen, and continue to seek them out. Though, they
will also continously fire, which gives their position away to the player if
the AI is approaching them.

Stationary AI -
This AI works almost like a turret. They stay staionary, as the
name entails, and spin around, looking for the player. If the
player is spotted, then the AI begins to shoot at them. After a set
amount of time, they go back to spinning around, looking for the
player.