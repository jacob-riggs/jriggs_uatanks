﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover : MonoBehaviour {

    private CharacterController cc;
    [HideInInspector] public Transform tf;
    private TankData data;

	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Move(Vector3 moveVector) {
        // Move in the direction and speed of vector
        cc.SimpleMove(moveVector.normalized * data.moveSpeed);
    }

    public void Turn(float turnSpeedAndDirection) {
        // Turn in direction of turnSpeedAndDirection
        tf.Rotate(0, turnSpeedAndDirection * Time.deltaTime, 0);
    }

    public void TurnTowards(Vector3 targetDirection) {
        // Find the rotation that looks down the vector of 'target direction'
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        // Rotate 'less than turnspeed' degrees towards targetRotation
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);
    }

}
