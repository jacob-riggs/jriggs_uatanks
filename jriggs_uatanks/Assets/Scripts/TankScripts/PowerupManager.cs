﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

    public List<Powerup> powerups;
    //Components
    private TankData data;

	// Use this for initialization
	void Start () {
        data = GetComponent<TankData>();
	}
	
	// Update is called once per frame
	void Update () {
        List<Powerup> powerupsToRemove = new List<Powerup>();

        foreach (Powerup powerup in powerups) {
            // Subtract last framedraw time
            powerup.countdown -= Time.deltaTime;
            // If it <= 0 remove the powerup
            if (powerup.countdown <= 0) {
                powerupsToRemove.Add(powerup);
            }
        }
        // Now that the foreach is done, we can remove the items
        foreach (Powerup powerup in powerupsToRemove) {
            RemovePowerup(powerup);
        }
	}

    public void AddPowerup(Powerup powerupToAdd) {
        Powerup newPowerup = new Powerup(powerupToAdd);
        powerups.Add(newPowerup);
        powerupToAdd.OnAddPowerup(data);
    }

    public void RemovePowerup(Powerup powerupToRemove) {
        powerups.Remove(powerupToRemove);
        powerupToRemove.OnRemovePowerup(data);
    }

}
