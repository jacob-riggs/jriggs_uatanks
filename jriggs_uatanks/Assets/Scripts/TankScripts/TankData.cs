﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TankMover))]
[RequireComponent(typeof(TankShooter))]

public class TankData : MonoBehaviour {
    
    // Tank data that affects gameplay
    public float moveSpeed;
    public float turnSpeed;
    public float shootSpeed;
    public float damage;
    public float bulletSpeed;
    public int currentScore;
    public int currentLives;
    public GameObject bulletPrefab;
    public string name;


    // Other tank components
    [HideInInspector] public TankMover mover;
    [HideInInspector] public TankShooter shooter;
    [HideInInspector] public TankHealth health;

    void Start() {
        mover = GetComponent<TankMover>();
        shooter = GetComponent<TankShooter>();
        health = GetComponent<TankHealth>();
        // Sets name to the name of the object
        name = this.gameObject.name;
    }
}
