﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour {

    public TankData data;
    public Transform shootPoint;
    private float nextShot;

    public AudioClip fire;

    // Get the options
    public Options options;

    // Use this for initialization
    void Start () {
        data = GetComponent<TankData>();
        options = GetComponent<Options>();

        nextShot = Time.time;
	}

    public void Shoot() {
        if (Time.time >= nextShot) {
            //Fire the shot
            GameObject bullet = Instantiate(data.bulletPrefab, shootPoint.position, shootPoint.rotation);

            // Play sound whenever shot is fired, and make sure it plays at the sfxVol set in options
            AudioSource.PlayClipAtPoint(fire, this.gameObject.transform.position, options.sfxVol);

            // Set the appropriate data
            BulletData bulletData = bullet.GetComponent<BulletData>();
            bulletData.damage = data.damage;
            bulletData.moveSpeed = data.bulletSpeed;
            bulletData.owner = GameObject.Find(data.gameObject.name);

            // Set next fire time
            nextShot = Time.time + data.shootSpeed;
        } else {
            // Do nothing
        }
    }
}
