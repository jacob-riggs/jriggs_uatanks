﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : MonoBehaviour {

    public GameObject objectToSpawn;
    public GameObject spawnedObject;
    private Transform tf;
    public float timeBetweenSpawns;
    private float spawnCountdown;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();

        spawnCountdown = timeBetweenSpawns;
	}
	
	// Update is called once per frame
	void Update () {
		if(spawnedObject == null) {
            // Countdown
            spawnCountdown -= Time.deltaTime;

            if (spawnCountdown <= 0) {
                // Spawn an object
                spawnedObject = Instantiate(objectToSpawn, tf.position, tf.rotation) as GameObject;

                // Rest Timer
                spawnCountdown = timeBetweenSpawns;
            }
        }
	}
}
