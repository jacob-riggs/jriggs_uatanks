﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{

    public GameObject playerPrefab;
    public GameObject player2Prefab;
    public GameObject player;
    public GameObject player2;
    public GameObject parent;

    public MapGenerator mapGenerator;

    public TankData data;

    public List<Vector3> playerSpawns = new List<Vector3>();

    // Use this for initialization
    IEnumerator Start() {

        data = GetComponent<TankData>();
        mapGenerator = GetComponent<MapGenerator>();

        yield return new WaitForSeconds(0f);

        foreach (Transform child in parent.transform) {
            foreach (Transform child2 in child.transform) {
                if (child2.name == "PlayerSpawn") {
                    playerSpawns.Add(child2.transform.position);
                }
            }
        }

        // Spawns in player 1 at beginning of game
        if (GameObject.Find("PlayerTank") == null) {
            int rand = Random.Range(0, playerSpawns.Count);
            player = Instantiate(playerPrefab, playerSpawns[rand], Quaternion.identity) as GameObject;
            player.name = "PlayerTank";
        }

        // Spawns in player 2 at beginning of game
        if (mapGenerator.splitScreen == true && GameObject.Find("PlayerTank2") == null) {
            int rand = Random.Range(0, playerSpawns.Count);
            player = Instantiate(player2Prefab, playerSpawns[rand], Quaternion.identity) as GameObject;
            player.name = "PlayerTank2";
        }
    }

    void Update() {

         //Moves player1 to a random player spawn when they are killed
        if (GameObject.FindGameObjectWithTag("Player1").GetComponent<TankData>().health.currentHealth == 0) {

            // Set player one tank as variable
            GameObject tankOne = GameObject.FindGameObjectWithTag("Player1");
            // Check if player has lives
            if (GameObject.FindGameObjectWithTag("Player1").GetComponent<TankData>().currentLives > 0) { 
                int rand = Random.Range(0, playerSpawns.Count);
                tankOne.gameObject.transform.position = playerSpawns[rand];
                tankOne.gameObject.GetComponent<TankData>().health.currentHealth = 100;
                tankOne.gameObject.GetComponent<TankData>().currentLives--;
            }
            else {
                Destroy(tankOne);
            }
        }

         //Moves player2 to a random player spawn when they are killed
        if (GameObject.FindGameObjectWithTag("Player2").GetComponent<TankData>().health.currentHealth == 0) {

            // Set player two tank as variable
            GameObject tankTwo = GameObject.FindGameObjectWithTag("Player2");
            if (GameObject.FindGameObjectWithTag("Player2").GetComponent<TankData>().currentLives > 0) {
                int rand = Random.Range(0, playerSpawns.Count);
                tankTwo.gameObject.transform.position = playerSpawns[rand];
                
                tankTwo.gameObject.GetComponent<TankData>().currentLives--;
                tankTwo.gameObject.GetComponent<TankData>().health.currentHealth = 100;
            }
            else {
                Destroy(tankTwo);
            }
        }
    }
}

