﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public GameObject aggressivePrefab;
    public GameObject hunterPrefab;
    public GameObject stationaryPrefab;
    public GameObject cowardPrefab;
    public GameObject parent;

    public List<Vector3> enemySpawns = new List<Vector3>();
    public List<GameObject> enemyTypes = new List<GameObject>();

    // Use this for initialization
    IEnumerator Start() {

        yield return new WaitForSeconds(1.5f);

        foreach (Transform child in parent.transform) {
            foreach (Transform child2 in child.transform) {
                if (child2.name == "EnemySpawn") {
                    enemySpawns.Add(child2.transform.position);
                }
            }
        }

        int rand = Random.Range(0, enemySpawns.Count);
        Instantiate(aggressivePrefab, enemySpawns[rand], Quaternion.identity);
        enemySpawns.Remove(enemySpawns[rand]);
        int rand2 = Random.Range(0, enemySpawns.Count);
        Instantiate(hunterPrefab, enemySpawns[rand2], Quaternion.identity);
        enemySpawns.Remove(enemySpawns[rand2]);
        int rand3 = Random.Range(0, enemySpawns.Count);
        Instantiate(stationaryPrefab, enemySpawns[rand3], Quaternion.identity);
        enemySpawns.Remove(enemySpawns[rand3]);
        int rand4 = Random.Range(0, enemySpawns.Count);
        Instantiate(cowardPrefab, enemySpawns[rand4], Quaternion.identity);
        enemySpawns.Remove(enemySpawns[rand4]);

        enemyTypes.Add(aggressivePrefab);
        enemyTypes.Add(hunterPrefab);
        enemyTypes.Add(stationaryPrefab);
        enemyTypes.Add(cowardPrefab);
    }

    void Update() {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Tank");
        if(enemies.Length < 4) {
            int randEnemy = Random.Range(0, enemyTypes.Count);
            int randSpawn = Random.Range(0, enemySpawns.Count);
            Instantiate(enemyTypes[randEnemy], enemySpawns[randSpawn], Quaternion.identity);
        }
    }
 
}
