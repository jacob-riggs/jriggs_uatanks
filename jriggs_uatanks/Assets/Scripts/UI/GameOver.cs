﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    // Get the tanks so that we can easily check to see if they exist
    public GameObject playerOne;
    public GameObject playerTwo;

    // Times for waiting in Update
    private float timer = 0f;
    public float waitingTime = 5f;

    IEnumerator Start() {
        //Wait to make sure players are spawned before setting them to the player variables
        yield return new WaitForSeconds(.5f);

        playerOne = GameObject.FindGameObjectWithTag("Player1");
        playerTwo = GameObject.FindGameObjectWithTag("Player2");
    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;

        if (timer > waitingTime) {
            if (playerOne == null && playerTwo == null) {
                SceneManager.LoadScene(2);
            }
        }
    }
}
