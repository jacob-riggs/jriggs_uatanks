﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour {

    // Make an Audio Source variable to grab the menu music source
    public AudioSource menuMusic;

    // Creates a variable and then gets the Options script for the value to easily grab the Load and Save functions
    public Options Options;

    void Start() {
        menuMusic = GetComponent<AudioSource>();
        Options = GetComponent<Options>();
    }

    // Update is called once per frame
    void Update () {
        menuMusic.volume = Options.musicVol;
	}
}
