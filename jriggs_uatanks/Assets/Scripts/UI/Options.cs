﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour {

    // Ints for the volume levels
    public float sfxVol;
    public float musicVol;

    // Toggle inputs for the checkboxes
    public Toggle mapOfDay;
    public Toggle splitScreen;

    // Ints so that mapOfDay and splitScreen can be saved
    public int mapOfDayInt;
    public int splitScreenInt;

	// Use this for initialization
	void Start () {
        Load();
	}
	
	// Update is called once per frame
	void Update () {
        // Logic to set mapofDay and SplitScreen ints
        if (mapOfDay.isOn) {
            mapOfDayInt = 1;
        }
        else {
            mapOfDayInt = 0;
        }
        if (splitScreen.isOn) {
            splitScreenInt = 1;
        }
        else {
            splitScreenInt = 0;
        }
	}

    // Functions for the volume buttons. Divide by 100 to get percentage amounts instead of whole numbers
    public void MusicUp() {
        musicVol += ((musicVol + 1) / 100);
    }
    public void MusicDown() {
        musicVol += ((musicVol - 1) / 100);
    }
    public void SfxUp() {
        sfxVol += ((sfxVol + 1) / 100);
    }
    public void SfxDown() {
        sfxVol += ((sfxVol - 1) / 100);
    }

    public void Save() {
        PlayerPrefs.SetFloat("MusicVol", musicVol);
        PlayerPrefs.SetFloat("SfxVol", sfxVol);
        PlayerPrefs.SetInt("MapOfDay", mapOfDayInt);
        PlayerPrefs.SetInt("SplitScreen", splitScreenInt);
        PlayerPrefs.Save();
    }

    public void Load() {
        musicVol = PlayerPrefs.GetFloat("MusicVol");
        sfxVol = PlayerPrefs.GetFloat("SfxVol");
        mapOfDayInt = PlayerPrefs.GetInt("MapOfDay");
        splitScreenInt = PlayerPrefs.GetInt("SplitScreen");
    }
}
