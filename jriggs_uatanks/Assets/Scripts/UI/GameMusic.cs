﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMusic : MonoBehaviour {

    // Make an Audio Source variable to grab the menu music source
    public AudioSource gameMusic;

    // Creates a variable and then gets the Options script for the value to easily grab the Load and Save functions
    public Options options;

    void Start() {
        gameMusic = GetComponent<AudioSource>();
        options = GetComponent<Options>();
    }

    // Update is called once per frame
    void Update() {
        gameMusic.volume = options.musicVol;
    }
}
