﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour {

    public AudioClip click;

    // Creates a variable and then gets the Options script for the value to easily grab the Load and Save functions
    public Options options;

    void Start() {
        options = GetComponent<Options>();
    }

    public void Quit() {
        options.Save();
        Application.Quit();
    }

}
