﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoader : MonoBehaviour {

    public AudioClip click;

    // Creates a variable and then gets the Options script for the value to easily grab the Load and Save functions
    public Options options;

    void Start() {
        options = GetComponent<Options>();
    }

    public void LoadScene() {
        options.Save();
        SceneManager.LoadScene(1);
    }

}
