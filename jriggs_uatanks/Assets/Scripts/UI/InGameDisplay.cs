﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameDisplay : MonoBehaviour {

    // Get UI text boxes for player scores and lives
    public Text playerOneScore;
    public Text playerOneLives;
    public Text playerTwoScore;
    public Text playerTwoLives;

    // Get the tanks so that we can easily grab their lives and score to put into the text
    public GameObject playerOne;
    public GameObject playerTwo;

    public MapGenerator mapGenerator;

    // Variables to change UI for different play modes
    public Canvas playerTwoCanvas;
    public Camera playerOneCamera;

	// Use this for initialization
	IEnumerator Start() {

        // Wait to make sure players are spawned before setting them to the player variables
        yield return new WaitForSeconds(0f);

        mapGenerator = GetComponent<MapGenerator>();

        // Get the player two canvas to delete if it is only one player
        GameObject tempObject = GameObject.Find("PlayerTwo");
        playerTwoCanvas = tempObject.GetComponent<Canvas>();

        // Get the component for the Player One Camera so that that it can be changed
        GameObject tempObject2 = GameObject.Find("Player 1");
        playerOneCamera = tempObject2.GetComponent<Camera>();

        playerOne = GameObject.FindGameObjectWithTag("Player1");
        playerTwo = GameObject.FindGameObjectWithTag("Player2");

        if (mapGenerator.splitScreen == false) {
            Destroy(playerTwoCanvas);
            playerOneCamera.rect = new Rect(0, 0, 1, 1);
        }
	}
	
	// Update is called once per frame
	void Update () {
        playerOneScore.text = "Score: " + playerOne.GetComponent<TankData>().currentScore;
        playerOneLives.text = "Lives: " + playerOne.GetComponent<TankData>().currentLives;
        playerTwoScore.text = "Score: " + playerTwo.GetComponent<TankData>().currentScore;
        playerTwoLives.text = "Lives: " + playerTwo.GetComponent<TankData>().currentLives;
        
    }
}
