﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupDamage : MonoBehaviour {

    public PowerupDamage powerup;

    public AudioClip pickup;

    private void OnTriggerEnter(Collider other) {
        PowerupManager pm = other.GetComponent<PowerupManager>();

        if (pm != null) {
            pm.AddPowerup(powerup);
            AudioSource.PlayClipAtPoint(pickup, other.gameObject.transform.position);
            Destroy(gameObject);
        }
    }

}
