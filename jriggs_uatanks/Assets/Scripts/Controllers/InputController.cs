﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

    public TankData pawn;
    public KeyCode forwardKey;
    public KeyCode rightTurn;
    public KeyCode leftTurn;
    public KeyCode backwardKey;
    public KeyCode shootKey;

	// Use this for initialization
	void Start () {
        GameManager.instance.player = this;
    }

    // Update is called once per frame
    void Update() {

        if (Input.GetKey(forwardKey)) {
            pawn.mover.Move(pawn.mover.transform.forward);
        }
        if (Input.GetKey(backwardKey)) {
            pawn.mover.Move(-pawn.mover.transform.forward);
        }
        if (Input.GetKey(rightTurn)) {
            pawn.mover.Turn(pawn.turnSpeed);
        }
        if (Input.GetKey(leftTurn)) {
            pawn.mover.Turn(-pawn.turnSpeed);
        }
        if (Input.GetKey(shootKey)) {
            pawn.shooter.Shoot();
        }
	}
}
