﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointAIController : AIContoller {

    public enum AIStates {Patrol, Chase, Dead};
    public AIStates currentState;
    private float timeInCurrentState = 0;
    public float chaseDistance = 10;
    public float chaseTime = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public override void Update () {
        // Update timier each frame
        timeInCurrentState += Time.deltaTime;

        // Switch based on current state
        switch (currentState) {
            // If I'm in that state
            case AIStates.Patrol:
                // Perform the actions for the state
                Patrol();
                // Check for the transitions
                if (Vector3.Distance(pawn.mover.tf.position, GameManager.instance.player.pawn.mover.tf.position) <= chaseDistance) {
                    ChangeState(AIStates.Chase);
                }
                break;
            case AIStates.Chase:
                Chase();
                if (timeInCurrentState > chaseTime) {
                    ChangeState(AIStates.Patrol);
                }
                break;
        }
	}
    public void ChangeState(AIStates newState) {
        // Set my state
        currentState = newState;
        // Reset my timer
        timeInCurrentState = 0;
    }
}
