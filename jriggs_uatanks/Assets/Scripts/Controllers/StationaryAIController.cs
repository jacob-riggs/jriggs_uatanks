﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationaryAIController : AIContoller {

    public enum AIStates {Spin, Shoot, Dead};
    public AIStates currentState;
    private float timeInCurrentState;
    public float seeDistance = 10;
    public float shootTime = 7;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public override void Update () {
        // Update timer every frame 
        timeInCurrentState += Time.deltaTime;

        // Switch based on current state
        switch (currentState) {
            case AIStates.Spin:
                // Peform the action for this state
                Spin();
                RaycastHit hit;
                Physics.SphereCast(pawn.mover.tf.position, .01f, pawn.mover.tf.forward, out hit, seeDistance);
                // Check for the transitions
                if (hit.collider != null && hit.collider.name == "PlayerTank") {
                    ChangeState(AIStates.Shoot);
                }
                break;
            case AIStates.Shoot:
                Shoot();
                if (timeInCurrentState >= shootTime) {
                    ChangeState(AIStates.Spin);
                }
                break;
        }
	}
    public void ChangeState(AIStates newState) {
        // Set my state
        currentState = newState;
        // Reset my timer
        timeInCurrentState = 0;
    }
}
