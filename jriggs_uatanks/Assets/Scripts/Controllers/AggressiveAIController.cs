﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggressiveAIController : AIContoller {

    public enum AIStates { Idle, Chase, Flee, Dead };
    public AIStates currentState;
    private float timeInCurrentState = 0;
    public float chaseDistance = 10;
    public float fleeHealthPercent = .5f;
    public float fleeTime = 10;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    public override void Update() {
        // Update the timer in each frame
        timeInCurrentState += Time.deltaTime;

        // Switch statement based off of the current state
        switch (currentState) {
            case AIStates.Idle:
                // Perform the actions for this state
                Idle();
                // Check for the transitions in this state
                if (Vector3.Distance(pawn.mover.tf.position, GameManager.instance.player.pawn.mover.tf.position) <= chaseDistance) { 
                    ChangeState(AIStates.Chase);
                }
                if ((pawn.health.currentHealth / pawn.health.maxHealth) < fleeHealthPercent) {
                    ChangeState(AIStates.Flee);
                }
                break;
            case AIStates.Chase:
                Chase();
                if ((pawn.health.currentHealth / pawn.health.maxHealth) < fleeHealthPercent) {
                    ChangeState(AIStates.Flee);
                }
                break;
            case AIStates.Flee:
                Flee();
                if (timeInCurrentState > fleeTime) {
                    ChangeState(AIStates.Idle);
                }
                break;
        }
    }

    public void ChangeState(AIStates newState) {
        // Set the state
        currentState = newState;
        // Reset the timer
        timeInCurrentState = 0;
    }

}
