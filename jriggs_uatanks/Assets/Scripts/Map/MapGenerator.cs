﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour {

    // Room variables. List of room prefabs and how big they will be
    public List<Room> rooms;
    public float tileXWidth;
    public float tileZWidth;

    // How many rooms will be spawned in
    public int numCols;
    public int numRows;

    // Seed options. Number to store seed and a boolean to set it as MapOfTheDay
    public int mapSeed;
    public bool isMapOfTheDay;

    // Bool to check if it is split screen or not
    public bool splitScreen;

    public List<GameObject> tilePrefabs;

    public Options options;

    // Use this for initialization
    void Start() {
        // Get the options component
        options = GetComponent<Options>();
        // Load in the PlayerPrefs
        options.Load();
        // Set isMapOfTheDay based on PlayerPrefs
        if (options.mapOfDayInt == 1) {
            isMapOfTheDay = true;
        }
        else {
            isMapOfTheDay = false;
        }
        // Set splitScreen based on PlayerPrefs
        if (options.splitScreenInt == 1) {
            splitScreen = true;
        }
        else {
            splitScreen = false;
        }

        // Check first to see if map of the day is wanted and set to day if yes
        if (isMapOfTheDay) {
            mapSeed = DateToInt(DateTime.Now.Date);
        }
        // If not map of the day, use the time to get map seed
        else if (mapSeed == 0) {
            mapSeed = DateToInt(DateTime.Now);
        }
        BuildMap();
    }

    // Update is called once per frame
    void Update() {

    }

    public int DateToInt(DateTime dateToUse) {
        // Add up date and return it
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Second + dateToUse.Millisecond;
    }

    public void BuildMap() {
        for (int currentRow = 0; currentRow < numRows; currentRow++) {
            for (int currentCol = 0; currentCol < numCols; currentCol++) {
                // Generate a random tile
                int rand = UnityEngine.Random.Range(0, tilePrefabs.Count);
                GameObject newTile = Instantiate(tilePrefabs[rand]) as GameObject;
                // name it
                newTile.name = "Tile (" + currentCol + "," + currentRow + ")";
                // Make this tile a child of THIS object
                newTile.transform.parent = this.transform;


                // These set of if statements will delete the needed "doors" so that the map is open
                if (newTile.gameObject.name == "Tile (0,0)") {
                    Destroy(newTile.transform.GetChild(9).gameObject);
                    Destroy(newTile.transform.GetChild(11).gameObject);
                }

                if (newTile.gameObject.name == "Tile (1,0)") {
                    Destroy(newTile.transform.GetChild(9).gameObject);
                    Destroy(newTile.transform.GetChild(11).gameObject);
                    Destroy(newTile.transform.GetChild(12).gameObject);
                }

                if (newTile.gameObject.name == "Tile (2,0)") {
                    Destroy(newTile.transform.GetChild(9).gameObject);
                    Destroy(newTile.transform.GetChild(12).gameObject);
                }

                if (newTile.gameObject.name == "Tile (0,1)") {
                    Destroy(newTile.transform.GetChild(9).gameObject);
                    Destroy(newTile.transform.GetChild(11).gameObject);
                    Destroy(newTile.transform.GetChild(10).gameObject);
                }

                if (newTile.gameObject.name == "Tile (1,1)") {
                    Destroy(newTile.transform.GetChild(9).gameObject);
                    Destroy(newTile.transform.GetChild(11).gameObject);
                    Destroy(newTile.transform.GetChild(12).gameObject);
                    Destroy(newTile.transform.GetChild(10).gameObject);
                }

                if (newTile.gameObject.name == "Tile (2,1)") {
                    Destroy(newTile.transform.GetChild(9).gameObject);
                    Destroy(newTile.transform.GetChild(10).gameObject);
                    Destroy(newTile.transform.GetChild(12).gameObject);
                }

                if (newTile.gameObject.name == "Tile (0,2)") {
                    Destroy(newTile.transform.GetChild(10).gameObject);
                    Destroy(newTile.transform.GetChild(11).gameObject);
                }

                if (newTile.gameObject.name == "Tile (1,2)") {
                    Destroy(newTile.transform.GetChild(10).gameObject);
                    Destroy(newTile.transform.GetChild(11).gameObject);
                    Destroy(newTile.transform.GetChild(12).gameObject);
                }

                if (newTile.gameObject.name == "Tile (2,2)") {
                    Destroy(newTile.transform.GetChild(10).gameObject);
                    Destroy(newTile.transform.GetChild(12).gameObject);
                }

                // move it into position
                float XPosition = currentCol * tileXWidth;
                float ZPosition = currentRow * tileZWidth;
                newTile.transform.localPosition = new Vector3(XPosition, 0.0f, ZPosition);
            }
        }
    }
}
