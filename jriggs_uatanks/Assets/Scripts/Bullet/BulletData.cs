﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour {

	public float moveSpeed = 1.0f;
    public float damage = 25.0f;
    public float lifespan = 3.0f;
    public TankData shooter = null;
    public GameObject owner = null;
    
    // Use this for initialization
	private void Start () {
        Destroy(gameObject, lifespan);
	}

}
