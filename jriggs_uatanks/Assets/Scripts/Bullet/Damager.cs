﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour {

    // Approptiate Data
    private BulletData bdata;
    public TankData data;

    // Audio Clips
    public AudioClip hit;
    public AudioClip death;

    // Spawner compnents
    public EnemySpawner enemySpawner;
    public PlayerSpawner playerSpawner;

    // Get the options
    public Options options;

    // Use this for initialization
    void Start () {
        bdata = GetComponent<BulletData>();
        data = GetComponent<TankData>();

        options = GetComponent<Options>();

        enemySpawner = GetComponent<EnemySpawner>();
        playerSpawner = GetComponent<PlayerSpawner>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Tank" || other.gameObject.tag == "Player1" || other.gameObject.tag == "Player2") {
            if (other.gameObject.GetComponent<TankData>().health.currentHealth <= 0) {
                if (other.gameObject.tag == "Tank") {
                    // Gets the owner of the bullet if they kill another tank and adds 100 points. Destroys object if it's an enemy
                    this.bdata.owner.gameObject.GetComponent<TankData>().currentScore += 100;
                    AudioSource.PlayClipAtPoint(death, other.gameObject.transform.position, options.sfxVol);
                    Destroy(other.gameObject);
                }
                else {
                    this.bdata.owner.gameObject.GetComponent<TankData>().currentScore += 100;
                    AudioSource.PlayClipAtPoint(death, other.gameObject.transform.position, options.sfxVol);
                }

            }

            else {
                other.gameObject.GetComponent<TankData>().health.currentHealth -= bdata.damage;
                Destroy(this.gameObject);
            }
        }
        else {
            AudioSource.PlayClipAtPoint(hit, other.gameObject.transform.position, options.sfxVol);
            Destroy(this.gameObject);
        }
    }
}
