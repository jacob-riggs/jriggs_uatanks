﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup {

    // Powerup Data
    public float bonusAmount;
    public float countdown;
    public bool isPerm;

    // Constructor
    public Powerup() { }
    public Powerup(Powerup powerupToClone) {
        bonusAmount = powerupToClone.bonusAmount;
        countdown = powerupToClone.countdown;
        isPerm = powerupToClone.isPerm;
    }

    public virtual void OnAddPowerup(TankData data) { }
    public virtual void OnRemovePowerup(TankData data) { }
    
}
