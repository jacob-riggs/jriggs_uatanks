﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupFireSpeed : Powerup {

    public override void OnAddPowerup(TankData data) {

        // Take away from Shoot Speed to allow to shoot faster
        data.shootSpeed -= bonusAmount;

        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup(TankData data) {

        base.OnRemovePowerup(data);

        // Add back to Shoot Speed
        data.shootSpeed += bonusAmount;
    }

}
