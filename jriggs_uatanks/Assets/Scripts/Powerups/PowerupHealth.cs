﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupHealth : Powerup {

	public override void OnAddPowerup (TankData data) {

        // Add to health
        data.health.currentHealth += bonusAmount;

        base.OnAddPowerup(data);

	}

    public override void OnRemovePowerup(TankData data) {

        if (isPerm)
            return;
        base.OnRemovePowerup(data);

        //Remove from health
        data.health.currentHealth -= bonusAmount;
    }

}
