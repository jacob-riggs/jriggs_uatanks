﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PowerupDamage : Powerup {

    public override void OnAddPowerup(TankData data) {
       
        // Add to damage
        data.damage += bonusAmount;

        base.OnAddPowerup(data);
    }

    public override void OnRemovePowerup(TankData data) {

        base.OnRemovePowerup(data);

        // Remove from damage
        data.damage -= bonusAmount;

    }

}
